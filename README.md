Important copyright information, please see below.

# Report issues here:
https://github.com/kangarko/ChatControl-Red/issues

# Download the latest build here:
https://github.com/kangarko/ChatControl-Red/releases

# Compiling:

1. Download Foundation from https://github.com/kangarko/foundation
2. Run Maven "clean install" in your IDE.
3. You're most welcome to contribute as long as you retain our formatting. If you use Eclipse, you can download our exact formatting with one click: https://github.com/kangarko/Foundation/blob/master/Eclipse%20Settings%20And%20Formatting.zip

The works distributed in the repositoriy is all rights reserved. That means no sharing, redistributing, reselling, both commercially or non-commercially or similar is allowed.
Exception is granted for the purpose of reviewing the code for vulneratibilities, compiling and testing it on your local computer and discussing the software in question in the GitHub link above. You can also learn from it to help you code as long as you do not reuse code from this repository in your own works.
There is also no warranty on this product and it is provided as-is. It is not production friendly.
Please note that you could be liable for damages (including costs and attorneys' fees) if you infringe our copyrights.