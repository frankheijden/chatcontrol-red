# Death messages. Configuration will follow later.

# They are read like a newspaper and each player only sees one message,
# that is the first one we can send to him.

# Require the player to by killed by an arrow.
#group playerArrow
#require projectile arrow
#require killer player
#message:
#- {player} has been murdered by {killer}'s [item]

# Require the player to by killed by a trident.
#group playerTrident
#require projectile trident
#require killer player
#message:
#- {player} has thrown [item] at {killer}

# Require the player to be killed by himself throwing an enderpearl.
#group enderPearl
#require self
#require cause fall
#require projectile ender_pearl
#message:
#- {player} has died by ender pearl

# Support MythicMobs or Boss plugin and send a special death message.
#group boss
#require boss *
#message:
#- {player} has died by a Boss {boss_name}

# If no group from above applies, fall back and send a default message.
#group default
#then log {player} has died at {world} {x} {y} {z} by {cause}
#message:
#- {player} has died by {cause} cause