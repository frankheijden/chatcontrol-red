# -----------------------------------------------------------------------------------------------
# This file applies rules to commands and includes rules from global.rs.
#
# For help, see https://github.com/kangarko/ChatControl-Red/wiki/Rules
# -----------------------------------------------------------------------------------------------

@import global

# -----------------------------------------------------------------------------------------------
# Prevent some abused game commands.
# -----------------------------------------------------------------------------------------------

# Prevent /op command but ignore other commands by ending the message with \b.
# This makes the filter match /op but not /open. The \b means basically a space after the word.
#match ^/op\b
#ignore perm chatcontrol.bypass.rules
#name /op
#then warn &bYes, we are giving away OPs. Seriously, what did you expect?!
#then notify chatcontrol.notify.rulesalert &8[&7ID {ruleID}&8] &7{player}: &f{message}
#then deny

# Prevent /gm and /gamemode command without permission, replacing error with a funny message.
#match ^/(gm|gamemode)\b
#ignore perm essentials.gamemode
#name /gm
#then warn &aThat would be nice, wouldn't it?
#then notify chatcontrol.notify.rulesalert &8[&7ID {ruleID}&8] &7{player}: &f{message}
#then deny

#match ^/(heal)\b
#ignore perm essentials.heal
#then warn &dGame too hard for ya?
#then deny

#match ^/trade\b
#ignore gamemode survival
#then warn &cNah, you cannot trade your hard earned creative diamonds fool!
#then deny

# -----------------------------------------------------------------------------------------------
# Create new commands and educate players.
# -----------------------------------------------------------------------------------------------

# Prevent sending mails directly to staff members and forward them to appropriate channel.
# This obviously requires you having /helpop system, it will not work if you don't have one.
# TIP: You can use channels to create a helpop channel, and create a rule that forwards
# all /helpop messages into such channel, all using just ChatControl. 
#match ^/mail send (kangarko|anotheradmin)\b
#then warn {prefix_warn}Please use &c/helpop&6 to contact our staff.
#then deny

# Catch "/helpop" command and send usage.
#match ^/helpop$
#dont verbose
#then warn &cDescription: Send a message to online staff members.
#then warn &cUsage: /helpop <message>
#then deny

# Catch "/helpop <message>" and redirect to the helpop channel.
#match ^(/helpop) (.*)
#dont verbose
#then command channel send helpop $2
#then deny

# Typing /help will call the "help" format showing players your help.
# You can also match multiple pages of help by creating multiple rules such as ^/help 2$
# and formats such as help-2 and design your entire help system using ChatControl! 
#match ^/help$
#then warn help
#then deny

# You can create new in-game commands such as /ping that is using a custom ping variable
# from the /variables folder.
#match ^/ping$
#then warn {prefix_info}Your ping is: {ping}ms
#then deny

# TODO Show people how to forward messages to channels

# -----------------------------------------------------------------------------------------------
# Example usage of group matching.
# -----------------------------------------------------------------------------------------------

# Regular expression will make multiple group matches, in this case
# (prefix) is $1 (first) and (.*) is $2 (second)
# You can refer to them in the messages.
# NOTICE: YOU CAN ALSO USE CHATCONTROL'S TAG SYSTEM TO SET A PREFIX VIA /TAG
#match ^/(prefix) (.*)
#require perm prefix.set.own &cInsufficient permission to change prefix. ({permission})
#then console pex user {player} prefix $2
#then warn Your prefix is now: $2
#then deny