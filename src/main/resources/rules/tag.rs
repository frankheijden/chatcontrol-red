#
# This file manages rules applying for player tags: nick, prefix and suffix
#

@import global

# Prevent certain words being used as nicks.
#match Notch
#require tag nick
#then warn {prefix_error} This tag has been disallowed!
#then deny

# Prevent nicks longer than the amount of matching letters, here 11.
#match .{11,}
#then warn {prefix_error} Cannot have nicks longer than 10 letters!
#then deny