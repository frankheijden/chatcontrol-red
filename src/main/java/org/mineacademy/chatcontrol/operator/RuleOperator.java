package org.mineacademy.chatcontrol.operator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nullable;

import org.bukkit.GameMode;
import org.bukkit.command.CommandSender;
import org.mineacademy.chatcontrol.ServerCache;
import org.mineacademy.chatcontrol.api.RuleReplaceEvent;
import org.mineacademy.chatcontrol.model.Format;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.PlayerUtil;
import org.mineacademy.fo.RandomUtil;
import org.mineacademy.fo.ReflectionUtil;
import org.mineacademy.fo.Valid;
import org.mineacademy.fo.collection.SerializedMap;
import org.mineacademy.fo.debug.Debugger;
import org.mineacademy.fo.exception.EventHandledException;
import org.mineacademy.fo.model.JavaScriptExecutor;
import org.mineacademy.fo.model.SimpleComponent;
import org.mineacademy.fo.model.SimpleTime;
import org.mineacademy.fo.model.Tuple;
import org.mineacademy.fo.visual.VisualizedRegion;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class RuleOperator extends Operator {

	/**
	 * Replace the matching expression in the message with the optional replacement
	 */
	private final Map<Pattern, String> beforeReplace = new HashMap<>();

	/**
	 * Permission required for the rule to apply,
	 * message sent to player if he lacks it.
	 */
	@Nullable
	private Tuple<String, String> requirePermission;

	/**
	 * JavaScript boolean output required to be true for the rule to apply
	 */
	@Nullable
	private String requireScript;

	/**
	 * World names to require
	 */
	private final Set<String> requireWorlds = new HashSet<>();

	/**
	 * Region names to require
	 */
	private final Set<String> requireRegions = new HashSet<>();

	/**
	 * Channels to require
	 */
	private final Set<String> requireChannels = new HashSet<>();

	/**
	 * Permission to bypass the rule
	 */
	@Nullable
	private String ignorePermission;

	/**
	 * The match that, if matched, will make the rule be ignored
	 */
	@Nullable
	private Pattern ignoreMatch;

	/**
	 * JavaScript boolean output when true for the rule to bypass
	 */
	@Nullable
	private String ignoreScript;

	/**
	 * A list of command labels including / that will be ignored.
	 */
	private final Set<String> ignoreCommands = new HashSet<>();

	/**
	 * Gamemodes to ignore
	 */
	private final Set<GameMode> ignoreGamemodes = new HashSet<>();

	/**
	 * World names to ignore
	 */
	private final Set<String> ignoreWorlds = new HashSet<>();

	/**
	 * Region names to ignore
	 */
	private final Set<String> ignoreRegions = new HashSet<>();

	/**
	 * List of channels to ignore matching from
	 */
	private final Set<String> ignoreChannels = new HashSet<>();

	/**
	 * List of strings to randomly select to replace the matching part of the message to
	 */
	private final Set<String> replacements = new HashSet<>();

	/**
	 * List of strings to randomly select to completely rewrite the whole message to
	 */
	private final Set<String> rewrites = new HashSet<>();

	/**
	 * List of strings blahblahblah but for each world differently
	 */
	private final Map<String, Set<String>> worldRewrites = new HashMap<>();

	/**
	 * List of messages to send back to player when rule matches
	 */
	private final List<String> warnMessages = new ArrayList<>();

	/**
	 * @see org.mineacademy.chatcontrol.operator.Operator#onParse(java.lang.String, java.lang.String, java.lang.String[])
	 */
	@Override
	protected boolean onParse(String param, String theRest, String[] args) {

		final List<String> theRestSplit = Arrays.asList(theRest.split("\\|"));

		if ("before replace".equals(param)) {
			final String[] split = theRest.split(" with ");
			final Pattern regex = Common.compilePattern(split[0]);
			final String replacement = split.length > 1 ? split[1] : "";

			this.beforeReplace.put(regex, replacement);
		}

		else if ("require perm".equals(param) || "require permission".equals(param)) {
			checkNotSet(this.requirePermission, "require perm");
			final String[] split = theRest.split(" ");

			this.requirePermission = new Tuple<>(split[0], split.length > 1 ? Common.joinRange(1, split) : null);
		}

		else if ("require script".equals(param)) {
			checkNotSet(this.requireScript, "require script");

			this.requireScript = theRest;
		}

		else if ("require world".equals(param) || "require worlds".equals(param))
			this.requireWorlds.addAll(theRestSplit);

		else if ("require region".equals(param) || "require regions".equals(param))
			this.requireRegions.addAll(theRestSplit);

		else if ("require channel".equals(param) || "require channels".equals(param))
			this.requireChannels.addAll(theRestSplit);

		else if ("ignore perm".equals(param) || "ignore permission".equals(param)) {
			checkNotSet(this.ignorePermission, "ignore perm");

			this.ignorePermission = theRest;
		}

		else if ("ignore string".equals(param)) {
			checkNotSet(this.ignoreMatch, "ignore string");

			this.ignoreMatch = Common.compilePattern(theRest);
		}

		else if ("ignore script".equals(param)) {
			checkNotSet(this.ignoreScript, "ignore script");

			this.ignoreScript = theRest;
		}

		else if ("ignore command".equals(param) || "ignore commands".equals(param)) {
			for (final String label : theRestSplit)
				this.ignoreCommands.add(label);
		}

		else if ("ignore gamemode".equals(param) || "ignore gamemodes".equals(param)) {
			for (final String modeName : theRestSplit) {
				final GameMode gameMode = ReflectionUtil.lookupEnum(GameMode.class, modeName);

				this.ignoreGamemodes.add(gameMode);
			}
		}

		else if ("ignore world".equals(param) || "ignore worlds".equals(param)) {
			this.ignoreWorlds.addAll(theRestSplit);
		}

		else if ("ignore region".equals(param) || "ignore regions".equals(param)) {
			this.ignoreRegions.addAll(theRestSplit);
		}

		else if ("ignore channel".equals(param) || "ignore channels".equals(param)) {
			this.ignoreChannels.addAll(theRestSplit);
		}

		else if ("then replace".equals(param)) {
			this.replacements.addAll(theRestSplit);
		}

		else if ("then rewrite".equals(param)) {
			this.rewrites.addAll(theRestSplit);
		}

		else if ("then rewritein".equals(param)) {
			final String[] split = theRest.split(" ");
			Valid.checkBoolean(split.length > 1, "wrong then rewritein syntax! Usage: <world> <message>");

			final String world = split[0];
			final List<String> messages = Arrays.asList(Common.joinRange(1, split).split("\\|"));

			this.worldRewrites.put(world, new HashSet<>(messages));

		} else if ("then warn".equals(param)) {
			this.warnMessages.addAll(theRestSplit);

		} else
			return false;

		return true;
	}

	/**
	 * Collect all options we have to debug
	 *
	 * @return
	 */
	@Override
	protected SerializedMap collectOptions() {
		return super.collectOptions().put(SerializedMap.ofArray(
				"Before Replace", this.beforeReplace,
				"Require Permission", this.requirePermission,
				"Require Script", this.requireScript,
				"Require Worlds", this.requireWorlds,
				"Require Regions", this.requireRegions,
				"Require Channels", this.requireChannels,
				"Ignore Permission", this.ignorePermission,
				"Ignore Match", this.ignoreMatch,
				"Ignore Script", this.ignoreScript,
				"Ignore Commands", this.ignoreCommands,
				"Ignore Gamemodes", this.ignoreGamemodes,
				"Ignore Worlds", this.ignoreWorlds,
				"Ignore Regions", this.ignoreRegions,
				"Ignore Channels", this.ignoreChannels,
				"Replacements", this.replacements,
				"Rewrites", this.rewrites,
				"World Rewrites", this.worldRewrites,
				"Warn Messages", this.warnMessages

		));
	}

	/* ------------------------------------------------------------------------------- */
	/* Classes */
	/* ------------------------------------------------------------------------------- */

	/**
	 * Represents a check that is implemented by this class
	 */
	public abstract static class RuleOperatorCheck<T extends RuleOperator> extends OperatorCheck<RuleOperator> {

		/**
		 * @param sender
		 * @param message
		 */
		protected RuleOperatorCheck(CommandSender sender, String message) {
			super(sender, message);
		}

		/**
		 * @see org.mineacademy.chatcontrol.operator.Operator.OperatorCheck#filter(org.mineacademy.chatcontrol.operator.Operator)
		 */
		@Override
		protected final void filter(RuleOperator operator) throws EventHandledException {
			if (canFilter(operator))
				onRuleFilter(operator);
		}

		protected abstract void onRuleFilter(RuleOperator operator);

		/**
		 * @see org.mineacademy.chatcontrol.model.Checkable#canFilter(org.bukkit.command.CommandSender, java.lang.String, org.mineacademy.chatcontrol.operator.Operator)
		 */
		@Override
		protected boolean canFilter(RuleOperator operator) {

			// ----------------------------------------------------------------
			// Require
			// ----------------------------------------------------------------

			if (operator.getRequirePermission() != null) {
				final String permission = operator.getRequirePermission().getKey();
				final String noPermissionMessage = operator.getRequirePermission().getValue();

				if (!PlayerUtil.hasPerm(sender, replaceVariables(permission, operator))) {
					if (noPermissionMessage != null) {
						Common.tell(sender, replaceVariables(noPermissionMessage, operator));

						throw new EventHandledException(true);
					}

					return false;
				}
			}

			if (operator.getRequireScript() != null) {
				final Object result = JavaScriptExecutor.run(replaceVariables(operator.getRequireScript(), operator), sender);
				Valid.checkBoolean(result instanceof Boolean, "require script condition must return boolean not " + result.getClass() + " for rule " + operator);

				if ((boolean) result == false)
					return false;
			}

			if (isPlayer) {
				if (!operator.getRequireWorlds().isEmpty() && !Valid.isInList(player.getWorld().getName(), operator.getRequireWorlds()))
					return false;

				if (!operator.getRequireRegions().isEmpty()) {
					final List<String> regions = Common.convert(ServerCache.getInstance().findRegions(player.getLocation()), VisualizedRegion::getName);
					boolean found = false;

					for (final String requireRegionName : operator.getRequireRegions())
						if (regions.contains(requireRegionName)) {
							found = true;

							break;
						}

					if (!found)
						return false;
				}
			}

			// ----------------------------------------------------------------
			// Ignore
			// ----------------------------------------------------------------

			if (operator.getIgnorePermission() != null && PlayerUtil.hasPerm(sender, replaceVariables(operator.getIgnorePermission(), operator)))
				return false;

			if (operator.getIgnoreMatch() != null && Common.regExMatch(operator.getIgnoreMatch(), message))
				return false;

			if (operator.getIgnoreScript() != null) {
				final Object result = JavaScriptExecutor.run(replaceVariables(operator.getIgnoreScript(), operator), sender);
				Valid.checkBoolean(result instanceof Boolean, "ignore script condition must return boolean not " + result.getClass() + " for rule " + operator);

				if ((boolean) result == true)
					return false;
			}

			if (isPlayer) {
				if (operator.getIgnoreGamemodes().contains(player.getGameMode()))
					return false;

				if (operator.getIgnoreWorlds().contains(player.getWorld().getName()))
					return false;

				for (final String playersRegion : Common.convert(ServerCache.getInstance().findRegions(player.getLocation()), VisualizedRegion::getName))
					if (operator.getIgnoreRegions().contains(playersRegion))
						return false;
			}

			return super.canFilter(operator);
		}

		/**
		 * Run given operators for the given message and return the updated message
		 */
		protected void executeOperators(RuleOperator operator, @Nullable Matcher matcher) throws EventHandledException {

			// Delay
			if (operator.getDelay() != null) {
				final SimpleTime time = operator.getDelay().getKey();
				final String message = operator.getDelay().getValue();

				final long now = System.currentTimeMillis();

				// Round the number due to Bukkit scheduler lags
				final long delay = Math.round((now - operator.getLastExecuted()) / 1000D);

				if (delay < time.getTimeSeconds()) {
					Debugger.debug("operator", "\tbefore delay: " + delay + " threshold: " + time.getTimeSeconds());

					cancel(message == null ? null : replaceVariables(message.replace("{delay}", (time.getTimeSeconds() - delay) + ""), operator));
				}

				operator.setLastExecuted(now);
			}

			if (matcher != null) {
				if (!operator.getReplacements().isEmpty()) {
					int count = 0;

					do {
						String replacement = RandomUtil.nextItem(operator.getReplacements());

						// Automatically prolong one-letter replacements
						if (replacement.startsWith("@prolong "))
							replacement = Common.duplicate(replacement.replace("@prolong ", ""), matcher.group().length());

						// Fix greedy filters eating up ending space
						final boolean endsWithSpace = matcher.group().charAt(matcher.group().length() - 1) == ' ';
						final String replacedMatch = replaceVariables(replacement, operator) + (endsWithSpace ? " " : "");

						// Call API
						final RuleReplaceEvent event = new RuleReplaceEvent(sender, message, replacedMatch, operator, false);

						if (!Common.callEvent(event))
							continue;

						// Replace the first occurrence. Replacing all would break the prolong functionality
						message = matcher.replaceFirst(event.getReplacedMatch());

						// Give the changed message back to the matcher to replace all occurences
						matcher.reset(message);

						// Stop after 20 matches to prevent infinite loop
					} while (matcher.find() && count++ < 20);
				}

				if (!operator.getRewrites().isEmpty()) {
					final String replacedMatch = replaceVariables(RandomUtil.nextItem(operator.getRewrites()), operator);

					// Call API
					final RuleReplaceEvent event = new RuleReplaceEvent(sender, message, replacedMatch, operator, false);

					if (Common.callEvent(event))
						message = event.getReplacedMatch();
				}
			}

			if (isPlayer)
				if (operator.getWorldRewrites().containsKey(player.getWorld().getName())) {
					final String replacedMatch = RandomUtil.nextItem(operator.getWorldRewrites().get(player.getWorld().getName()));

					// Call API
					final RuleReplaceEvent event = new RuleReplaceEvent(sender, message, replacedMatch, operator, false);

					if (Common.callEvent(event))
						message = event.getReplacedMatch();
				}

			// Dirty: Run later including when EventHandledException is thrown
			Common.runLater(1, () -> {
				if (!operator.getWarnMessages().isEmpty() && !receivedAnyWarningMessage) {
					for (final String warnMessage : operator.getWarnMessages()) {
						final Format format = Format.parse(replaceVariables(warnMessage, operator));
						final SimpleComponent component = format.build(sender, message);

						component.send(sender);
					}
				}
			});

			super.executeOperators(operator);
		}
	}
}
