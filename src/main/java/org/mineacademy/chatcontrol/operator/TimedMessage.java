package org.mineacademy.chatcontrol.operator;

import org.mineacademy.chatcontrol.settings.Settings;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.model.SimpleTime;
import org.mineacademy.fo.model.Tuple;

/**
 * Represents join, leave, kick or timed message broadcast
 */
public final class TimedMessage extends PlayerMessage {

	/**
	 * @param group
	 */
	public TimedMessage(String group) {
		super(Type.TIMED, group);
	}

	/**
	 * Return the broadcast specific delay or the default one
	 *
	 * @return
	 */
	@Override
	public Tuple<SimpleTime, String> getDelay() {
		return Common.getOrDefault(super.getDelay(), new Tuple<>(Settings.Messages.TIMED_DELAY, null));
	}

	/**
	 * Return the prefix or the default one if not set
	 *
	 * @return the prefix
	 */
	@Override
	public String getPrefix() {
		return super.getPrefix() != null ? super.getPrefix() : Settings.Messages.TIMED_PREFIX;
	}

	/**
	 * Return the suffix or the default one if not set
	 *
	 * @return the suffix
	 */
	@Override
	public String getSuffix() {
		return super.getSuffix() != null ? super.getSuffix() : Settings.Messages.TIMED_SUFFIX;
	}
}