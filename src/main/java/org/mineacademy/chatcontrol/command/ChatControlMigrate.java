package org.mineacademy.chatcontrol.command;

import java.util.List;

import org.mineacademy.chatcontrol.command.ChatControlCommands.ChatControlSubCommand;
import org.mineacademy.chatcontrol.model.Permissions;
import org.mineacademy.chatcontrol.model.UserMap;
import org.mineacademy.chatcontrol.settings.Lang;
import org.mineacademy.chatcontrol.settings.Settings;
import org.mineacademy.fo.Common;

public final class ChatControlMigrate extends ChatControlSubCommand {

	public ChatControlMigrate() {
		super("migrate");

		setUsage(Lang.of("Commands.Migrate.Usage"));
		setDescription(Lang.of("Commands.Migrate.Description"));
		setMinArguments(1);
		setPermission(Permissions.Command.MIGRATE);
	}

	/**
	 * @see org.mineacademy.fo.command.SimpleCommand#getMultilineUsageMessage()
	 */
	@Override
	protected String[] getMultilineUsageMessage() {
		return Lang.ofArray("Commands.Migrate.Usages");
	}

	/**
	 * @see org.mineacademy.fo.command.SimpleCommand#onCommand()
	 */
	@Override
	protected void execute() {
		checkUsage(args.length == 1);

		final String param = args[0];

		if ("chc8".equals(param))
			returnTell("Migration is not yet implemented!");

		else if ("import".equals(param))
			Common.runAsync(() -> {
				checkBoolean(Settings.MySQL.ENABLED, Lang.of("Commands.No_MySQL"));

				tellInfo(Lang.of("Commands.Migrate.Import_Start"));
				UserMap.getInstance().importFromDb();

				tellSuccess(Lang.of("Commands.Migrate.Import_Finish"));
			});

		else if ("export".equals(param))
			Common.runAsync(() -> {
				checkBoolean(Settings.MySQL.ENABLED, Lang.of("Commands.No_MySQL"));

				tellInfo(Lang.of("Commands.Migrate.Export_Start"));
				final int exportedEntryCount = UserMap.getInstance().exportToDb();

				tellSuccess(Lang.of("Commands.Migrate.Export_Finish", exportedEntryCount));
			});

		else if ("essentials".equals(param)) {
			UserMap.getInstance().importEssentialsAndSave();

			tellSuccess(Lang.of("Commands.Migrate.Essentials"));
		}

		else
			returnInvalidArgs();
	}

	/**
	 * @see org.mineacademy.fo.command.SimpleCommand#tabComplete()
	 */
	@Override
	protected List<String> tabComplete() {

		if (args.length == 1)
			return completeLastWord("chc8", "import", "export", "essentials");

		return NO_COMPLETE;
	}
}
