package org.mineacademy.chatcontrol.command;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.chatcontrol.PlayerCache;
import org.mineacademy.chatcontrol.SenderCache;
import org.mineacademy.chatcontrol.ServerCache;
import org.mineacademy.chatcontrol.command.ChatControlCommands.ChatControlCommand;
import org.mineacademy.chatcontrol.model.Book;
import org.mineacademy.chatcontrol.model.Bungee.BungeePacket;
import org.mineacademy.chatcontrol.model.Log;
import org.mineacademy.chatcontrol.model.Mail;
import org.mineacademy.chatcontrol.model.Mail.Recipient;
import org.mineacademy.chatcontrol.model.Permissions;
import org.mineacademy.chatcontrol.model.Spy;
import org.mineacademy.chatcontrol.model.Toggle;
import org.mineacademy.chatcontrol.model.UserMap;
import org.mineacademy.chatcontrol.settings.Lang;
import org.mineacademy.chatcontrol.settings.Settings;
import org.mineacademy.fo.BungeeUtil;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.Messenger;
import org.mineacademy.fo.MinecraftVersion;
import org.mineacademy.fo.MinecraftVersion.V;
import org.mineacademy.fo.TimeUtil;
import org.mineacademy.fo.collection.SerializedMap;
import org.mineacademy.fo.conversation.SimplePrompt;
import org.mineacademy.fo.model.ChatPaginator;
import org.mineacademy.fo.model.SimpleComponent;
import org.mineacademy.fo.model.Tuple;
import org.mineacademy.fo.model.Variables;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.CompMetadata;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

public final class CommandMail extends ChatControlCommand implements Listener {

	public CommandMail() {
		super(Settings.Mail.COMMAND_ALIASES);

		setUsage(Lang.of("Commands.Mail.Usage"));
		setDescription(Lang.of("Commands.Mail.Description"));
		setMinArguments(1);
		setPermission(Permissions.Command.MAIL);

		Common.registerEvents(this);
	}

	/**
	 * @see org.mineacademy.fo.command.SimpleCommand#getMultilineUsageMessage()
	 */
	@Override
	protected String[] getMultilineUsageMessage() {
		return Lang.ofArray("Commands.Mail.Usages");
	}

	/**
	 * @see org.mineacademy.fo.command.SimpleCommand#onCommand()
	 */
	@Override
	protected void execute() {
		checkBoolean(MinecraftVersion.atLeast(V.v1_8), Lang.of("Commands.Incompatible", "1.8.8"));
		checkConsole();

		final String param = args[0];
		final Player player = getPlayer();
		final UUID uuid = player.getUniqueId();

		final ServerCache serverCache = ServerCache.getInstance();
		final SenderCache senderCache = SenderCache.from(sender);
		final PlayerCache playerCache = PlayerCache.from(player);

		final Book pendingBody = Common.getOrDefault(senderCache.getPendingMail(), Book.newEmptyBook());

		if ("send".equals(param) || "s".equals(param)) {
			checkNotNull(senderCache.getPendingMail(), Lang.of("Commands.Mail.No_Pending"));
			checkBoolean(pendingBody.isSigned(), Lang.of("Commands.Mail.No_Subject"));
			checkArgs(2, Lang.of("Commands.Mail.No_Recipients"));
			checkUsage(args.length == 2);

			sendMail(args[1], pendingBody);

			return;
		}

		else if ("autor".equals(param) || "ar".equals(param)) {
			checkArgs(2, Lang.of("Commands.Mail.Autoresponder_Usage"));

			final String timeRaw = joinArgs(1);

			if ("off".equals(timeRaw) || "view".equals(timeRaw)) {
				checkBoolean(playerCache.isAutoResponderValid(), Lang.of("Commands.Mail.Autoresponder_Disabled"));

				final Tuple<Book, Long> autoResponder = playerCache.getAutoResponder();

				if ("off".equals(timeRaw)) {
					playerCache.removeAutoResponder();

					tellSuccess(Lang.of("Commands.Mail.Autoresponder_Removed"));
				} else {
					autoResponder.getKey().open(player);

					tellSuccess(Lang.of("Commands.Mail.Autoresponder").replace("{title}", autoResponder.getKey().getTitle()).replace("{date}", TimeUtil.getFormattedDateShort(autoResponder.getValue())));
				}

				return;
			}

			final long futureTime = System.currentTimeMillis() + (findTime(timeRaw).getTimeSeconds() * 1000);

			// If has no email, try updating current auto-responder's date
			if (senderCache.getPendingMail() == null) {
				checkBoolean(playerCache.hasAutoResponder(), Lang.of("Commands.Mail.Autoresponder_Missing"));

				playerCache.setAutoResponderDate(futureTime);

				tellSuccess(Lang.of("Commands.Mail.Autoresponder_Updated", TimeUtil.getFormattedDateShort(futureTime)));
				return;
			}

			checkBoolean(pendingBody.isSigned(), Lang.of("Commands.Mail.No_Subject"));

			// Save
			playerCache.setAutoResponder(senderCache.getPendingMail(), futureTime);

			// Remove draft from cache because it was finished
			senderCache.setPendingMail(null);

			tellSuccess(Lang.of("Commands.Mail.Autoresponder_Set", TimeUtil.getFormattedDateShort(futureTime)));
			return;
		}

		else if ("open".equals(param) || "forward".equals(param) || "delete-sender".equals(param) || "delete-recipient".equals(param)) {
			checkArgs(2, "Unique mail ID has not been provided. If this is a bug, please report it. If you are playing hard, play harder!");

			final UUID mailId = UUID.fromString(args[1]);
			final Mail mail = serverCache.findMail(mailId);
			checkNotNull(mail, Lang.of("Commands.Mail.Delete_Invalid"));

			if ("open".equals(param)) {
				mail.open(player);

				if (!String.join(" ", args).contains("-donotmarkasread"))
					serverCache.markOpen(mail, player);

				BungeeUtil.tellBungee(BungeePacket.MAIL_SYNC, serverCache.getMailsSerialized());

				return;

			} else if ("forward".equals(param)) {

				// No recipients
				if (args.length == 2)
					new PromptForwardRecipients(mailId).show(player);

				else if (args.length == 3)
					sendMail(args[2], Book.clone(mail.getBody(), player.getName()));

				else
					returnInvalidArgs();

				return;
			}

			else if ("delete-sender".equals(param)) {
				checkBoolean(!mail.isSenderDeleted(), Lang.of("Commands.Mail.Delete_Invalid"));

				mail.setSenderDeleted(true);
				serverCache.save();

				BungeeUtil.tellBungee(BungeePacket.MAIL_SYNC, serverCache.getMailsSerialized());

				tellSuccess(Lang.of("Commands.Mail.Delete_Sender"));
				return;
			}

			else if ("delete-recipient".equals(param)) {
				checkArgs(3, Lang.of("Commands.Mail.Delete_No_Recipient"));

				final UUID recipientId = UUID.fromString(args[2]);
				final Recipient recipient = mail.findRecipient(recipientId);
				checkNotNull(recipient, Lang.of("Commands.Mail.Delete_Invalid_Recipient"));

				checkBoolean(!recipient.isMarkedDeleted(), Lang.of("Commands.Mail.Delete_Invalid"));

				recipient.setMarkedDeleted(true);
				serverCache.save();

				BungeeUtil.tellBungee(BungeePacket.MAIL_SYNC, serverCache.getMailsSerialized());

				tellSuccess(Lang.of("Commands.Mail.Delete_Recipient"));
				return;
			}
		}

		checkUsage(args.length == 1);

		if ("new".equals(param) || "n".equals(param)) {
			checkUsage(args.length == 1);
			checkBoolean(CompMaterial.isAir(player.getItemInHand().getType()), Lang.of("Commands.Mail.Hand_Full"));

			for (final ItemStack stack : player.getInventory().getContents())
				if (stack != null && CompMetadata.hasMetadata(stack, Book.TAG))
					returnTell(Lang.of("Commands.Mail.Already_Drafting"));

			final ItemStack bookItem = pendingBody.toEditableBook(Lang.of("Commands.Mail.Item_Title"), Lang.ofArray("Commands.Mail.Item_Tooltip"));

			player.setItemInHand(bookItem);
			tellInfo(Lang.ofScript("Commands.Mail.New_Usage", SerializedMap.of("noPendingMail", senderCache.getPendingMail() == null)));

		} else if ("inbox".equals(param) || "i".equals(param) || "read".equals(param)) {

			final List<SimpleComponent> pages = new ArrayList<>();

			for (final Mail incoming : serverCache.findMailsTo(uuid)) {
				final Recipient recipient = incoming.findRecipient(uuid);
				final boolean read = recipient.hasOpened();

				// Hide deleted emails
				if (recipient.isMarkedDeleted())
					continue;

				pages.add(SimpleComponent
						.of("&8[&2O&8]")
						.onHover(Lang.of("Commands.Mail.Open_Tooltip"))
						.onClickRunCmd("/" + getLabel() + " open " + incoming.getUniqueId())

						.append(" ")

						.append("&8[" + (read ? "&7R" : "&eU") + "&8]")
						.onHover(Lang.ofScript("Commands.Mail.Read_Tooltip", SerializedMap.of("read", read), TimeUtil.getFormattedDateShort(recipient.getOpenTime())))

						.append(" ")

						.append("&8[&3F&8]")
						.onHover(Lang.of("Commands.Mail.Forward_Tooltip"))
						.onClickRunCmd("/" + getLabel() + " forward " + incoming.getUniqueId())

						.append(" ")

						.append("&8[&cX&8]")
						.onHover(Lang.of("Commands.Mail.Delete_Tooltip"))
						.onClickRunCmd("/" + getLabel() + " delete-recipient " + incoming.getUniqueId() + " " + player.getUniqueId())

						.append(Lang.of("Commands.Mail.Inbox_Line")
								.replace("{subject}", incoming.getTitle())
								.replace("{sender}", incoming.getSenderName())
								.replace("{date}", TimeUtil.getFormattedDateMonth(incoming.getSentDate()))));
			}

			checkBoolean(!pages.isEmpty(), Lang.of("Commands.Mail.No_Incoming_Mail"));

			new ChatPaginator()
					.setFoundationHeader(Lang.of("Commands.Mail.Inbox_Header"))
					.setPages(pages)
					.send(player);

		} else if ("archive".equals(param) || "a".equals(param) || "sent".equals(param)) {
			final List<SimpleComponent> pages = new ArrayList<>();

			for (final Mail outgoing : serverCache.findMailsFrom(uuid)) {

				// Hide deleted emails
				if (outgoing.isSenderDeleted() || outgoing.isAutoReply())
					continue;

				final List<String> statusHover = new ArrayList<>();
				statusHover.add(Lang.of("Commands.Mail.Archive_Recipients_Tooltip"));

				for (final Recipient recipient : outgoing.getRecipients()) {
					final String recipientName = UserMap.getInstance().getName(recipient.getUniqueId());

					if (recipientName != null)
						statusHover.add(Lang.ofScript("Commands.Mail.Archive_Read_Tooltip", SerializedMap.of("hasOpened", recipient.hasOpened()), recipientName));
				}

				pages.add(SimpleComponent.empty()

						.append("&8[&6O&8]")
						.onHover(Lang.of("Commands.Mail.Open_Tooltip"))
						.onClickRunCmd("/" + getLabel() + " open " + outgoing.getUniqueId() + " -donotmarkasread")

						.append(" ")

						.append("&8[&3F&8]")
						.onHover(Lang.of("Commands.Mail.Forward_Tooltip"))
						.onClickRunCmd("/" + getLabel() + " forward " + outgoing.getUniqueId())

						.append(" ")

						.append("&8[&cX&8]")
						.onHover(Lang.of("Commands.Mail.Delete_Tooltip"))
						.onClickRunCmd("/" + getLabel() + " delete-sender " + outgoing.getUniqueId())

						.append(Lang.of("Commands.Mail.Archive_Line")
								.replace("{subject}", outgoing.getTitle())
								.replace("{recipients}", outgoing.getRecipients().size() + Lang.of("Commands.Mail.Archive_Recipient"))
								.replace("{date}", TimeUtil.getFormattedDateMonth(outgoing.getSentDate())))

						.onHover(statusHover));
			}

			checkBoolean(!pages.isEmpty(), Lang.of("Commands.Mail.Archive_No_Mail"));

			new ChatPaginator()
					.setFoundationHeader(Lang.of("Commands.Mail.Archive_Header"))
					.setPages(pages)
					.send(player);

		} else
			returnInvalidArgs();
	}

	/*
	 * Parses the recipients and sends the book email
	 */
	private void sendMail(String recipientsLine, Book body) {
		final Set<UUID> recipients = new HashSet<>();
		final String[] split = recipientsLine.split("\\|");
		final List<PlayerCache> recipientCaches = new ArrayList<>();

		for (int i = 0; i < split.length; i++) {
			final String recipientName = split[i];
			final boolean last = i + 1 == split.length;

			pollCache(recipientName, recipientCache -> {
				final UUID recipientId = recipientCache.getUniqueId();

				checkBoolean(hasPerm(Permissions.Bypass.REACH) || !recipientCache.isIgnoringPlayer(getPlayer().getUniqueId()), Lang.of("Commands.Mail.Send_Fail_Ignore", recipientName));

				// Allow sending to self even if ignored
				if (Settings.Toggle.APPLY_ON.contains(Toggle.MAIL) && !getPlayer().getUniqueId().equals(recipientCache.getUniqueId()))
					checkBoolean(!recipientCache.isIgnoringPart(Toggle.MAIL), Lang.of("Commands.Mail.Send_Fail_Toggle", recipientName));

				recipients.add(recipientId);
				recipientCaches.add(recipientCache);

				// Auto responder
				if (recipientCache.isAutoResponderValid() && !recipientId.equals(getPlayer().getUniqueId()))
					Common.runLater(20, () -> Mail.sendAutoReply(getPlayer().getUniqueId(), recipientId, recipientCache.getAutoResponder().getKey()));

				if (last) {
					// Prepare mail
					final Mail mail = Mail.send(getPlayer().getUniqueId(), recipients, body);

					// Broadcast to spying players
					Spy.broadcastMail(getPlayer(), body.getTitle(), recipients, mail.getUniqueId());

					// Log
					Log.logMail(getPlayer(), mail);

					// Remove draft from cache because it was finished
					SenderCache.from(sender).setPendingMail(null);

					tellSuccess(Lang.of("Commands.Mail.Send_Success"));
				}
			});
		}
	}

	/**
	 * @see org.mineacademy.fo.command.SimpleCommand#tabComplete()
	 */
	@Override
	protected List<String> tabComplete() {

		if (args.length == 1)
			return completeLastWord("new", "n", "send", "s", "autor", "inbox", "i", "read", "archive", "a", "sent");

		if (args.length == 2)
			if ("autor".equals(args[0]))
				return completeLastWord("view", "off", "3 hours", "7 days");
			else if ("send".equals(args[0]))
				return completeLastWordPlayerNames();

		return NO_COMPLETE;
	}

	/* ------------------------------------------------------------------------------- */
	/* Event handlers */
	/* ------------------------------------------------------------------------------- */

	/**
	 * Monitor player dropping the draft and remove it.
	 *
	 * @param event
	 */
	@EventHandler
	public void onItemDrop(PlayerDropItemEvent event) {
		final ItemStack item = event.getItemDrop().getItemStack();
		final Player player = event.getPlayer();

		if (CompMetadata.hasMetadata(item, Book.TAG)) {
			discardBook(player, event);

			Common.runLater(() -> player.setItemInHand(new ItemStack(CompMaterial.AIR.getMaterial())));
		}
	}

	/**
	 * Monitor player clicking anywhere holding the draft and remove it.
	 *
	 * @param event
	 */
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		final Player player = (Player) event.getWhoClicked();
		final ItemStack clicked = event.getCurrentItem();
		final ItemStack cursor = event.getCursor();

		if ((cursor != null && CompMetadata.hasMetadata(player, Book.TAG)) || (clicked != null && CompMetadata.hasMetadata(clicked, Book.TAG))) {
			event.setCursor(new ItemStack(CompMaterial.AIR.getMaterial()));
			event.setCurrentItem(new ItemStack(CompMaterial.AIR.getMaterial()));

			discardBook(player, event);
		}
	}

	/*
	 * Discards the pending mail if any
	 */
	private void discardBook(Player player, Cancellable event) {
		event.setCancelled(true);

		SenderCache.from(player).setPendingMail(null);
		Messenger.info(player, Lang.of("Commands.Mail.Draft_Discarded"));

		Common.runLater(() -> player.updateInventory());
	}

	/* ------------------------------------------------------------------------------- */
	/* Classes */
	/* ------------------------------------------------------------------------------- */

	@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
	private static class PromptForwardRecipients extends SimplePrompt {

		/**
		 * The mail that is being forwarded
		 */
		private final UUID mailId;

		/**
		 * @see org.mineacademy.fo.conversation.SimplePrompt#getPrompt(org.bukkit.conversations.ConversationContext)
		 */
		@Override
		protected String getPrompt(ConversationContext ctx) {
			return Variables.replace(Lang.of("Commands.Mail.Forward_Recipients"), getPlayer(ctx));
		}

		/**
		 * @see org.mineacademy.fo.conversation.SimplePrompt#isInputValid(org.bukkit.conversations.ConversationContext, java.lang.String)
		 */
		@Override
		protected boolean isInputValid(ConversationContext context, String input) {

			if (input.isEmpty() || input.equals("|")) {
				tell(Variables.replace(Lang.of("Commands.Mail.Forward_Recipients_Invalid", input), getPlayer(context)));

				return false;
			}

			for (final String part : input.split("\\|")) {
				if (!UserMap.getInstance().isPlayerStored(part)) {
					tell(Messenger.getErrorPrefix() + Lang.of("Player.Not_Stored", part));

					return false;
				}
			}

			return true;
		}

		/**
		 * @see org.bukkit.conversations.ValidatingPrompt#acceptValidatedInput(org.bukkit.conversations.ConversationContext, java.lang.String)
		 */
		@Override
		protected Prompt acceptValidatedInput(ConversationContext context, String input) {

			// Send later so that the player is not longer counted as "conversing"
			Common.runLater(5, () -> {
				Common.dispatchCommandAsPlayer(getPlayer(context), Variables.replace("{label_mail} forward " + this.mailId + " " + input, getPlayer(context)));

				tell(context, Variables.replace(Lang.of("Commands.Mail.Forward_Success"), getPlayer(context)));
			});

			return null;
		}
	}
}
