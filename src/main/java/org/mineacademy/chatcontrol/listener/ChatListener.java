package org.mineacademy.chatcontrol.listener;

import java.util.Iterator;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.mineacademy.chatcontrol.PlayerCache;
import org.mineacademy.chatcontrol.SenderCache;
import org.mineacademy.chatcontrol.model.Channel;
import org.mineacademy.chatcontrol.model.Checker;
import org.mineacademy.chatcontrol.model.Colors;
import org.mineacademy.chatcontrol.model.Mute;
import org.mineacademy.chatcontrol.model.Newcomer;
import org.mineacademy.chatcontrol.model.Permissions;
import org.mineacademy.chatcontrol.settings.Lang;
import org.mineacademy.chatcontrol.settings.Settings;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.Messenger;
import org.mineacademy.fo.PlayerUtil;
import org.mineacademy.fo.ReflectionUtil;
import org.mineacademy.fo.event.SimpleListener;

import lombok.Getter;

/**
 * The main listener for async chat
 */
public final class ChatListener extends SimpleListener<AsyncPlayerChatEvent> {

	/**
	 * The singleton instance
	 */
	@Getter
	private static final ChatListener instance = new ChatListener();

	/*
	 * Creates new listener
	 */
	private ChatListener() {
		super(AsyncPlayerChatEvent.class, Settings.FORMATTER_PRIORITY, true);
	}

	/**
	 * @see org.mineacademy.fo.event.SimpleListener#execute(org.bukkit.event.Event)
	 */
	@Override
	protected void execute(AsyncPlayerChatEvent event) {
		final Player player = event.getPlayer();
		final PlayerCache cache = PlayerCache.from(player);
		final SenderCache senderCache = SenderCache.from(player);
		final Set<Player> recipients = event.getRecipients();
		String message = event.getMessage();

		checkBoolean(!senderCache.isLoadingMySQL(), Lang.of("Data_Loading"));
		checkPerm(Permissions.Chat.WRITE, Lang.of("Player.No_Write_Chat_Permission", Permissions.Chat.WRITE));

		// Newcomer
		if (Settings.Newcomer.RESTRICT_CHAT && Newcomer.isNewcomer(player) && !Settings.Newcomer.RESTRICT_CHAT_WHITELIST.isInList(message))
			cancel(Lang.of("Player.Newcomer_Cannot_Write"));

		// Remove recipients who can't read the message
		for (final Iterator<Player> it = recipients.iterator(); it.hasNext();) {
			final Player recipient = it.next();

			if (!PlayerUtil.hasPerm(recipient, Permissions.Chat.READ) || (Settings.Newcomer.RESTRICT_SEEING_CHAT && Newcomer.isNewcomer(recipient)))
				it.remove();
		}

		// Auto conversation mode
		if (cache.getConversingPlayer() != null) {
			final Player conversingWith = Bukkit.getPlayerExact(cache.getConversingPlayer());
			final String finalMessage = message;

			if (conversingWith != null && conversingWith.isOnline()) {
				// Must invoke the chat() method to apply rules and filtering
				Common.runLater(() -> player.chat("/" + Settings.PrivateMessages.TELL_ALIASES.get(0) + " " + conversingWith.getName() + " " + finalMessage));

			} else {
				Messenger.warn(player, Lang.of("Commands.Tell.Conversation_Offline", cache.getConversingPlayer()));

				Common.runLater(() -> cache.setConversingPlayer(null));
			}

			cancel();
		}

		// Do not use channels
		if (!Settings.Channels.ENABLED || Settings.Channels.IGNORE_WORLDS.contains(player.getWorld().getName())) {

			// Mute
			checkBoolean(!Mute.isChatMuted(player), Lang.of("Commands.Mute.Cannot_Chat"));

			final Checker checker = Checker.filterChat(player, message, null);

			if (checker.isCancelledSilently())
				recipients.removeIf(recipient -> !recipient.getName().equals(player.getName()));

			// Update message from antispam/rules
			message = checker.getMessage();

			// Apply colors
			message = Colors.colorizeMessage(player, message);

			// Remove ignored players
			recipients.removeIf(recipient -> {

				// Prevent recipients on worlds where channels are enabled from seeing the message
				if (Settings.Channels.ENABLED && !Settings.Channels.IGNORE_WORLDS.contains(recipient.getWorld().getName()))
					return true;

				if (Settings.Ignore.ENABLED && Settings.Ignore.HIDE_CHAT && !hasPerm(Permissions.Bypass.REACH) && PlayerCache.from(recipient).isIgnoringPlayer(player.getUniqueId()))
					return true;

				return false;
			});

			// Update the message
			event.setMessage(message);

			return;
		}

		final Channel writeChannel = cache.getWriteChannel();

		checkPerm(Permissions.Chat.WRITE);
		checkNotNull(writeChannel, Lang.of(Channel.canJoinAnyChannel(player) ? "Player.No_Channel" : "Player.No_Possible_Channel"));

		// Prevent accidental typing
		if (PlayerUtil.isVanished(player))
			cancel(Lang.of("Player.Cannot_Chat_Vanished"));

		// Send to channel and return the edited message
		final Channel.Result result = writeChannel.sendMessage(player, message);

		// Act as cancel at the pipeline
		if (result.isCancelledSilently()) {
			event.setCancelled(true);

			return;
		}

		// Update the message for other plugins
		event.setMessage(result.getMessage());

		// Clear recipient list so that Bukkit does not send anyone the message
		// but other plugins can still catch the event.
		// By this time we already sent the message in our own way using interactive chat.
		recipients.clear();

		// Do not log to the console
		//if (MinecraftVersion.newerThan(V.v1_8))
		ReflectionUtil.setDeclaredField(event, "format", null);
		//else
		//	event.setFormat("");
	}
}
