package org.mineacademy.chatcontrol.listener;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.mineacademy.chatcontrol.PlayerCache;
import org.mineacademy.chatcontrol.model.Permissions;
import org.mineacademy.chatcontrol.model.Spy;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.PlayerUtil;
import org.mineacademy.fo.ReflectionUtil;
import org.mineacademy.fo.collection.SerializedMap;
import org.mineacademy.fo.model.HookManager;
import org.mineacademy.fo.plugin.SimplePlugin;
import org.mineacademy.fo.remain.Remain;

import com.gmail.nossr50.chat.author.Author;
import com.gmail.nossr50.events.chat.McMMOPartyChatEvent;
import com.palmergames.bukkit.TownyChat.channels.Channel;
import com.palmergames.bukkit.TownyChat.events.AsyncChatHookEvent;

import net.sacredlabyrinth.phaed.simpleclans.ClanPlayer;
import net.sacredlabyrinth.phaed.simpleclans.events.ChatEvent;

/**
 * A common listener for all third party plugin integration
 */
public final class ThirdPartiesListener {

	/**
	 * Register all compatible hooks
	 */
	public static void registerEvents() {

		if (Common.doesPluginExist("SimpleClans")) {
			Common.log("Note: Hooked into SimpleClans to filter ignored players");

			Common.registerEvents(new SimpleClansListener());
		}

		if (Common.doesPluginExist("TownyChat")) {
			Common.log("Note: Hooked into TownyChat to spy channels");

			Common.registerEvents(new TownyChatListener());
		}

		if (HookManager.isMcMMOLoaded()) {
			Common.log("Note: Hooked into mcMMO to spy channels");

			Common.registerEvents(new McMMOListener());
		}
	}
}

/**
 * SimpleClans handle
 */
final class SimpleClansListener implements Listener {

	/**
	 * Listen to simple clans chat and remove receivers who ignore the sender
	 *
	 * @param event
	 */
	@EventHandler
	public void onPlayerClanChat(ChatEvent event) {
		try {
			final ClanPlayer sender = event.getSender();

			// Reach message to players who ignore the sender if sender has bypass reach permission
			if (PlayerUtil.hasPerm(sender.toPlayer(), Permissions.Bypass.REACH))
				return;

			for (final Iterator<ClanPlayer> it = event.getReceivers().iterator(); it.hasNext();) {
				final ClanPlayer receiver = it.next();
				final PlayerCache receiverCache = PlayerCache.from(receiver.toPlayer());

				if (receiverCache.isIgnoringPlayer(sender.getUniqueId()))
					it.remove();
			}

		} catch (final IncompatibleClassChangeError ex) {
			Common.log("&cWarning: Processing message from SimpleClans failed, if you have TownyChat latest version contact "
					+ SimplePlugin.getNamed() + " authors to update their hook. The error was: " + ex);
		}
	}
}

/**
 * TownyChat handle
 */
final class TownyChatListener implements Listener {

	/**
	 * Listen to chat in towny channels and broadcast spying
	 *
	 * @param event
	 */
	@EventHandler
	public void onChat(AsyncChatHookEvent event) {
		try {
			final Player player = event.getPlayer();
			final String message = event.getMessage();
			final Channel channel = event.getChannel();

			Spy.broadcastMessage(player, message, Common.newSet(player.getUniqueId()), SerializedMap.of("channel", channel.getName()));

		} catch (final IncompatibleClassChangeError ex) {
			Common.log("&cWarning: Processing message from TownyChat channel failed, if you have TownyChat latest version contact "
					+ SimplePlugin.getNamed() + " authors to update their hook. The error was: " + ex);
		}
	}
}

/**
 * mcMMO handle
 */
final class McMMOListener implements Listener {

	/**
	 * Listen to party chat message and forward them to spying players
	 *
	 * @param event
	 */
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onPartyChat(McMMOPartyChatEvent event) {
		try {
			final String message = event.getMessage();
			final String party = event.getParty();

			CommandSender sender;

			try {
				final Author author = event.getAuthor();

				sender = author.isConsole() ? Bukkit.getConsoleSender() : Remain.getPlayerByUUID(author.uuid());

			} catch (final LinkageError ex) {
				sender = Bukkit.getPlayerExact(ReflectionUtil.invoke("getSender", event));
			}

			if (sender != null) {
				final Set<UUID> ignoredPlayers = sender instanceof Player ? Common.newSet(((Player) sender).getUniqueId()) : new HashSet<>();

				Spy.broadcastMessage(sender, message, ignoredPlayers, SerializedMap.of("channel", party));
			}
		} catch (final IncompatibleClassChangeError ex) {
			Common.log("&cWarning: Processing party chat from mcMMO failed, if you have mcMMO latest version contact "
					+ SimplePlugin.getNamed() + " authors to update their hook. The error was: " + ex);
		}
	}
}
