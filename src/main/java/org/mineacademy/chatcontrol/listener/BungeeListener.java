package org.mineacademy.chatcontrol.listener;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.mineacademy.chatcontrol.PlayerCache;
import org.mineacademy.chatcontrol.ServerCache;
import org.mineacademy.chatcontrol.SyncedCache;
import org.mineacademy.chatcontrol.model.Announce;
import org.mineacademy.chatcontrol.model.Announce.AnnounceType;
import org.mineacademy.chatcontrol.model.Bungee.BungeePacket;
import org.mineacademy.chatcontrol.model.Channel;
import org.mineacademy.chatcontrol.model.ListPlayers;
import org.mineacademy.chatcontrol.model.Mail;
import org.mineacademy.chatcontrol.model.Packets;
import org.mineacademy.chatcontrol.model.Players;
import org.mineacademy.chatcontrol.model.Spy;
import org.mineacademy.chatcontrol.model.Toggle;
import org.mineacademy.chatcontrol.model.UserMap;
import org.mineacademy.chatcontrol.settings.Settings;
import org.mineacademy.chatcontrol.settings.Settings.MySQL;
import org.mineacademy.fo.BungeeUtil;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.Messenger;
import org.mineacademy.fo.bungee.message.IncomingMessage;
import org.mineacademy.fo.collection.SerializedMap;
import org.mineacademy.fo.constants.FoConstants;
import org.mineacademy.fo.exception.FoException;
import org.mineacademy.fo.model.HookManager;
import org.mineacademy.fo.model.SimpleComponent;
import org.mineacademy.fo.model.SimpleTime;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.Remain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Represents the connection to BungeeCord
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class BungeeListener extends org.mineacademy.fo.bungee.BungeeListener {

	/**
	 * The singleton of this class
	 */
	@Getter
	private final static BungeeListener instance = new BungeeListener();

	/*
	 * The presently read packet
	 */
	private BungeePacket packet;

	/*
	 * The server the packet is coming from
	 */
	private String server;

	/*
	 * The sender of the packet
	 */
	private UUID senderUid;

	/**
	 * @see org.mineacademy.fo.bungee.BungeeListener#onMessageReceived(org.bukkit.entity.Player, org.mineacademy.fo.bungee.message.IncomingMessage)
	 */
	@Override
	public void onMessageReceived(Player player, IncomingMessage input) {

		this.packet = input.getAction();
		this.server = input.getServerName();
		this.senderUid = input.getSenderUid();

		if (this.packet != BungeePacket.PLAYERS_SYNC)
			System.out.println("@received " + this.packet + " from " + server);

		if (this.packet == BungeePacket.CHANNEL) {

			final String channelName = input.readString();
			final String senderName = input.readString();
			final UUID senderUUID = input.readUUID();
			final String message = input.readString();
			final SimpleComponent component = createComponent(input.readString());
			final String consoleLog = input.readString();
			final boolean hasMuteBypass = input.readBoolean();
			final boolean hasIgnoreBypass = input.readBoolean();
			final boolean hasLogBypass = input.readBoolean();

			final Channel channel = Channel.findChannel(channelName);

			if (Settings.Channels.ENABLED && Channel.isChannelLoaded(channelName))
				channel.processBungeeMessage(senderName, senderUUID, message, component, consoleLog, hasMuteBypass, hasIgnoreBypass, hasLogBypass);
		}

		else if (this.packet == BungeePacket.REMOVE_MESSAGE) {
			final UUID messageId = input.readUUID();

			if (HookManager.isProtocolLibLoaded())
				Packets.getInstance().removeMessage(messageId);
		}

		else if (this.packet == BungeePacket.SPY) {
			final Spy.Type type = input.readEnum(Spy.Type.class);
			final String channelName = input.readString();
			final String message = input.readString();
			final SimpleComponent component = createComponent(input.readString());
			final Set<UUID> ignoredPlayers = Common.convertSet(Remain.fromJsonList(input.readString()), UUID::fromString);

			Spy.broadcastFromBungee(type, channelName, message, component, ignoredPlayers);
		}

		else if (this.packet == BungeePacket.TOAST) {
			final UUID receiverUid = input.readUUID();
			final Toggle toggle = input.readEnum(Toggle.class);
			final String message = input.readString();
			final CompMaterial material = input.readEnum(CompMaterial.class);
			final Player receiver = Remain.getPlayerByUUID(receiverUid);

			if (receiver != null && receiver.isOnline())
				sendToast(player, toggle, message, material);
		}

		else if (this.packet == BungeePacket.PLAYERS_SYNC) {
			final SerializedMap mergedData = input.readMap();

			SyncedCache.upload(mergedData);
		}

		else if (this.packet == BungeePacket.DB_UPDATE) {
			final String playerName = input.readString();
			final UUID uniqueId = input.readUUID();
			final String nick = input.readString();
			final SerializedMap data = input.readMap();
			final String message = input.readString();

			final Player online = Remain.getPlayerByUUID(uniqueId);

			if (MySQL.ENABLED) {
				UserMap.getInstance().cacheLocally(new UserMap.Record(playerName, uniqueId, nick.isEmpty() ? null : nick));
				PlayerCache.loadOrUpdateCache(playerName, uniqueId, data);

				if (online != null && !message.isEmpty())
					Messenger.info(online, message);
			}
		}

		else if (this.packet == BungeePacket.MAIL_SYNC) {
			final SerializedMap mailsMap = input.readMap();

			// Also reload mails just in case
			final Set<Mail> mails = new HashSet<>();

			for (final Object mailData : mailsMap.values()) {
				final Mail mail = Mail.deserialize(SerializedMap.fromJson(mailData.toString()));

				mails.add(mail);
			}

			ServerCache.getInstance().cacheMailsLocally(mails);
		}

		else if (this.packet == BungeePacket.ANNOUNCEMENT) {
			final AnnounceType type = input.readEnum(AnnounceType.class);
			final String message = input.readString();
			final SerializedMap params = input.readMap();

			Announce.sendFromBungee(type, message, params);
		}

		else if (this.packet == BungeePacket.FORWARD_COMMAND) {
			final String server = input.readString();
			final String command = Common.colorize(input.readString().replace("{server_name}", server));

			if (server.equalsIgnoreCase(Remain.getServerName()))
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
		}

		else if (this.packet == BungeePacket.LIST_PLAYERS_REQUEST) {
			final String requesterUUID = input.readString();
			input.readString(); // server to request from, unused here
			final String prefix = input.readString();

			BungeeUtil.tellBungee(BungeePacket.LIST_PLAYERS_RESPONSE, requesterUUID, ListPlayers.compilePlayers(prefix));
		}

		else if (this.packet == BungeePacket.LIST_PLAYERS_RESPONSE) {
			final UUID requester = input.readUUID();
			final SerializedMap mergedData = input.readMap();
			final CommandSender requestingPlayer = requester.equals(FoConstants.NULL_UUID) ? Bukkit.getConsoleSender() : Remain.getPlayerByUUID(requester);

			if (requestingPlayer != null)
				ListPlayers.listPlayersFromBungee(requestingPlayer, mergedData);
		}

		else if (this.packet == BungeePacket.CLEAR_CHAT) {
			final String announceMessage = input.readString();
			Players.clearChatFromBungee();

			if (!announceMessage.isEmpty())
				Messenger.broadcastAnnounce(announceMessage);
		}

		else if (this.packet == BungeePacket.ME) {
			final UUID senderId = input.readUUID();
			final boolean reachBypass = input.readBoolean();
			final SimpleComponent component = SimpleComponent.deserialize(input.readMap());

			Players.showMe(senderId, reachBypass, component);
		}

		else if (this.packet == BungeePacket.MOTD) {
			final UUID receiverId = input.readUUID();
			final Player receiver = Remain.getPlayerByUUID(receiverId);

			if (receiver != null)
				Players.showMotd(receiver);
		}

		else if (this.packet == BungeePacket.NOTIFY) {
			final String permission = input.readString();
			final SimpleComponent component = SimpleComponent.deserialize(input.readMap());

			Players.broadcastWithPermission(permission, component);
		}

		else if (this.packet == BungeePacket.PLAIN_BROADCAST) {
			final String plainMessage = input.readString();

			for (final Player online : Remain.getOnlinePlayers())
				Common.tellNoPrefix(online, plainMessage);
		}

		else if (this.packet == BungeePacket.PLAIN_MESSAGE) {
			final UUID receiver = input.readUUID();
			final String message = input.readString();
			final Player online = Remain.getPlayerByUUID(receiver);

			if (online != null)
				Common.tellNoPrefix(online, message);
		}

		else if (this.packet == BungeePacket.SIMPLECOMPONENT_MESSAGE) {
			final UUID receiver = input.readUUID();
			final SimpleComponent component = SimpleComponent.deserialize(input.readMap());
			final Player online = Remain.getPlayerByUUID(receiver);

			if (online != null)
				component.send(online);
		}

		else if (this.packet == BungeePacket.MUTE) {
			final String type = input.readString();
			final String object = input.readString();
			final String durationRaw = input.readString();
			final String announceMessage = input.readString();

			final boolean isOff = "off".equals(durationRaw);
			final SimpleTime duration = isOff ? null : SimpleTime.from(durationRaw);

			if (type.equals("channel")) {
				final Channel channel = Channel.findChannel(object);

				if (channel != null) {
					channel.setMuted(duration);

					for (final Player online : channel.getOnlinePlayers().keySet())
						Common.tellNoPrefix(online, announceMessage);
				}
			}

			else if (type.equals("server")) {
				ServerCache.getInstance().setMuted(duration);

				for (final Player online : Remain.getOnlinePlayers())
					Common.tellNoPrefix(online, announceMessage);

			} else
				throw new FoException("Unhandled mute packet type " + type);
		}

		else
			throw new FoException("Unhandled packet from BungeeControl: " + this.packet);
	}

	/*
	 * Sends a toast message to the player given he is not ignoring it nor the sender
	 */
	private void sendToast(Player player, Toggle toggle, String message, CompMaterial material) {
		final PlayerCache cache = PlayerCache.from(player);

		if (!cache.isIgnoringPart(toggle) && !cache.isIgnoringPlayer(this.senderUid))
			Remain.sendToast(player, message, material);
	}

	/*
	 * Add bungee prefix
	 */
	private SimpleComponent createComponent(String json) {

		// Read the component
		final SimpleComponent component = SimpleComponent.deserialize(SerializedMap.fromJson(json));

		// Replace prefix variables
		final String prefix = Settings.Integration.BungeeCord.PREFIX.replace("{bungee_server_name}", this.server);

		return component.appendFirst(SimpleComponent.of(prefix));
	}
}