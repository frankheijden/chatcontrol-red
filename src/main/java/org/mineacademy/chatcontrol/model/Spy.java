package org.mineacademy.chatcontrol.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;

import javax.annotation.Nullable;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.chatcontrol.PlayerCache;
import org.mineacademy.chatcontrol.api.SpyEvent;
import org.mineacademy.chatcontrol.model.Bungee.BungeePacket;
import org.mineacademy.chatcontrol.settings.Lang;
import org.mineacademy.chatcontrol.settings.Settings;
import org.mineacademy.chatcontrol.settings.Settings.Integration.BungeeCord;
import org.mineacademy.fo.BungeeUtil;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.PlayerUtil;
import org.mineacademy.fo.Valid;
import org.mineacademy.fo.collection.SerializedMap;
import org.mineacademy.fo.model.SimpleComponent;
import org.mineacademy.fo.remain.Remain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Class holding methods for the /spy command
 */
public final class Spy {

	/**
	 * Broadcast a PM
	 *
	 * @param sender
	 * @param receiver
	 * @param message
	 */
	public static void broadcastPrivateMessage(UUID sender, UUID receiver, String message) {
		broadcastMessage(Type.PRIVATE_MESSAGE, null, null, message, SerializedMap.ofArray("sender", sender, "receiver", receiver), null, Common.newSet(sender, receiver));
	}

	/**
	 * Broadcast a mail
	 *
	 * @param initiator
	 * @param title
	 * @param receivers
	 * @param mailId
	 */
	public static void broadcastMail(Player initiator, String title, Set<UUID> receivers, UUID mailId) {
		final SerializedMap variables = SerializedMap.ofArray("sender", initiator, "receivers", Common.join(receivers), "mail_title", title, "mail_uuid", mailId.toString());

		receivers.add(initiator.getUniqueId());

		broadcastMessage(Type.MAIL, null, initiator, "", variables, null, receivers);
	}

	/**
	 * Broadcast a sign being edited to all spying players
	 *
	 * @param initiator
	 * @param lines joined lines with \n character
	 */
	public static void broadcastSign(Player initiator, String lines) {
		broadcastMessage(Type.SIGN, initiator, lines.replace("\n", " "), SerializedMap.ofArray("sign_lines", lines));
	}

	/**
	 * Broadcast a book being edited to all spying players
	 *
	 * @param initiator
	 * @param uuid the unique ID of the book used for display
	 */
	public static void broadcastBook(Player initiator, UUID uuid) {
		broadcastMessage(Type.BOOK, initiator, "", SerializedMap.ofArray("book_uuid", uuid.toString()));
	}

	/**
	 * Broadcast an item just renamed (not too long ago) on anvil to all spying players
	 *
	 * @param initiator
	 * @param item
	 */
	public static void broadcastAnvil(Player initiator, ItemStack item) {
		broadcastMessage(Type.ANVIL, null, initiator, "", new SerializedMap(), compound -> {
			final SimpleComponent itemComponent = SimpleComponent.of(Remain.getI18NDisplayName(item)).onHover(item);

			return compound.append(itemComponent);
		}, null);
	}

	/**
	 * Broadcast a message from a chat channel to all recipients
	 *
	 * @param sender
	 * @param message
	 * @param recipients
	 */
	public static void broadcastChannel(Channel channel, CommandSender sender, String message, Set<UUID> ignoredPlayers) {
		broadcastMessage(Type.CHAT, channel, sender, message, new SerializedMap(), null, ignoredPlayers);
	}

	/**
	 * Broadcast a general message
	 *
	 * @param sender
	 * @param message
	 * @param ignoredPlayers
	 * @param variables
	 */
	public static void broadcastMessage(CommandSender sender, String message, Set<UUID> ignoredPlayers, SerializedMap variables) {
		broadcastMessage(Type.CHAT, null, sender, message, variables, null, ignoredPlayers);
	}

	/**
	 * Broadcast a type message to all spying players
	 *
	 * @param type
	 * @param initiator
	 * @param message
	 */
	public static void broadcastMessage(Spy.Type type, CommandSender initiator, String message) {
		broadcastMessage(type, initiator, message, new SerializedMap());
	}

	/*
	 * A convenience shortcut function
	 */
	private static void broadcastMessage(Type type, CommandSender initiator, String message, SerializedMap variables) {
		broadcastMessage(type, null, initiator, message, variables, null, initiator instanceof Player ? Common.newSet(((Player) initiator).getUniqueId()) : null);
	}

	/*
	 * Broadcast spying message to all spying players
	 */
	private static void broadcastMessage(Type type, @Nullable Channel channel, @Nullable CommandSender initiator, String message, SerializedMap variables, @Nullable Function<SimpleComponent, SimpleComponent> postEdit, @Nullable Set<UUID> ignoredPlayers) {
		Valid.checkNotNull(type.getFormat(), "Type " + type + " has no format!");

		if (!Settings.Spy.APPLY_ON.contains(type))
			return;

		if (initiator != null && PlayerUtil.hasPerm(initiator, Permissions.Bypass.SPY)) {
			Log.logOnce("spy-bypass", "Note: Not sending " + initiator.getName() + "'s " + type + " to spying players because he had '" + Permissions.Bypass.SPY + "' permission." +
					" Player messages with such permission are not spied on. To disable that, give him this permission as negative (a false value if using LuckPerms).");

			return;
		}

		if (type == Type.COMMAND) {
			final String label = message.split(" ")[0];

			if (!Settings.Spy.COMMANDS.isInList(label))
				return;
		}

		// Place default variables
		if (initiator instanceof Player)
			variables.put("location", Common.shortLocation(((Player) initiator).getLocation()));

		if (channel != null)
			variables.put("channel", channel.getName());

		// Build component
		final boolean noPrefix = type.getFormat().startsWith("@noprefix ");
		final SimpleComponent prefix = Format.parse(Settings.Spy.PREFIX).build(initiator, message, variables);
		final SimpleComponent main = Format.parse(noPrefix ? type.getFormat().substring(9).trim() : type.getFormat()).build(initiator, message, variables);

		SimpleComponent compounded = noPrefix ? main : main.appendFirst(prefix);

		if (postEdit != null)
			compounded = postEdit.apply(compounded);

		// Send
		final List<Player> spyingPlayers = channel != null ? getOnlineSpyingChannelPlayers(channel) : getOnlineSpyingPlayers(type);

		// Remove ignored
		if (ignoredPlayers != null)
			spyingPlayers.removeIf(spyingPlayer -> ignoredPlayers.contains(spyingPlayer.getUniqueId()));

		final SpyEvent event = new SpyEvent(type, initiator, message, new HashSet<>(spyingPlayers));

		// API call
		if (Common.callEvent(event)) {

			// Update data from event
			final String finalMessage = event.getMessage();
			spyingPlayers.clear();
			spyingPlayers.addAll(event.getRecipients());

			// Broadcast
			for (final Player spyingPlayer : spyingPlayers) {
				if (initiator instanceof Player && spyingPlayer.equals(initiator))
					continue;

				compounded.send(spyingPlayer);
				ignoredPlayers.add(spyingPlayer.getUniqueId());
			}

			final SimpleComponent finalCompounded = compounded;

			// Send home to BungeeCord!
			if (BungeeCord.ENABLED)
				BungeeUtil.tellBungee(
						BungeePacket.SPY,
						type.getKey(), channel == null ? "" : channel.getName(),
						finalMessage,
						finalCompounded.serialize().toJson(),
						Remain.toJson(ignoredPlayers == null ? new HashSet<>() : Common.convert(ignoredPlayers, UUID::toString)));
		}
	}

	/**
	 * Processes a spy message from BungeeCord
	 *
	 * @param type
	 * @param channelName
	 * @param message
	 * @param component
	 * @param ignoredPlayers
	 */
	public static void broadcastFromBungee(Type type, String channelName, String message, SimpleComponent component, Set<UUID> ignoredPlayers) {

		if (!Settings.Spy.APPLY_ON.contains(type))
			return;

		if (type == Type.COMMAND) {
			final String label = message.split(" ")[0];

			if (!Settings.Spy.COMMANDS.isInList(label))
				return;
		}

		// Send
		final Channel channel = !channelName.isEmpty() ? Channel.findChannel(channelName) : null;
		final List<Player> spyingPlayers = channel != null ? getOnlineSpyingChannelPlayers(channel) : getOnlineSpyingPlayers(type);

		// Remove ignored
		spyingPlayers.removeIf(spyingPlayer -> {
			if (ignoredPlayers != null && ignoredPlayers.contains(spyingPlayer.getUniqueId()))
				return true;

			if (channel != null && channel.isInChannel(spyingPlayer))
				return true;

			return false;
		});

		final SpyEvent event = new SpyEvent(type, null, message, new HashSet<>(spyingPlayers));

		// API call
		if (Common.callEvent(event)) {

			// Update data from event
			message = event.getMessage();
			spyingPlayers.clear();
			spyingPlayers.addAll(event.getRecipients());

			// Broadcast
			component.send(spyingPlayers);
		}
	}

	/*
	 * Return list of online spying players in channel
	 */
	private static List<Player> getOnlineSpyingChannelPlayers(Channel channel) {
		final List<Player> spying = new ArrayList<>();

		for (final Player online : Remain.getOnlinePlayers())
			if (PlayerCache.from(online).isSpyingChannel(channel))
				spying.add(online);

		return spying;
	}

	/*
	 * Return list of online spying players
	 */
	private static List<Player> getOnlineSpyingPlayers(Spy.Type type) {
		final List<Player> spying = new ArrayList<>();

		for (final Player online : Remain.getOnlinePlayers())
			if (PlayerCache.from(online).getSpyingSectors().contains(type))
				spying.add(online);

		return spying;
	}

	/**
	 * Represents a rule type
	 */
	@RequiredArgsConstructor
	public enum Type {

		/**
		 * Spying channel messages
		 */
		CHAT("chat") {
			@Override
			public String getFormat() {
				return Settings.Spy.FORMAT_CHAT;
			}

			@Override
			public String getLocalized() {
				return Lang.of("Commands.Spy.Type_Chat");
			}
		},

		/**
		 * Spying player commands
		 */
		COMMAND("command") {
			@Override
			public String getFormat() {
				return Settings.Spy.FORMAT_COMMAND;
			}

			@Override
			public String getLocalized() {
				return Lang.of("Commands.Spy.Type_Command");
			}
		},

		/**
		 * Your mom n NSA spying private conversations yay!
		 */
		PRIVATE_MESSAGE("private_message") {
			@Override
			public String getFormat() {
				return Settings.Spy.FORMAT_PRIVATE_MESSAGE;
			}

			@Override
			public String getLocalized() {
				return Lang.of("Commands.Spy.Type_Private_Message");
			}
		},

		/**
		 * Spying mails when sent
		 */
		MAIL("mail") {
			@Override
			public String getFormat() {
				return Settings.Spy.FORMAT_MAIL;
			}

			@Override
			public String getLocalized() {
				return Lang.of("Commands.Spy.Type_Mail");
			}
		},

		/**
		 * Spying signs
		 */
		SIGN("sign") {
			@Override
			public String getFormat() {
				return Settings.Spy.FORMAT_SIGN;
			}

			@Override
			public String getLocalized() {
				return Lang.of("Commands.Spy.Type_Sign");
			}
		},

		/**
		 * Spying writing to books
		 */
		BOOK("book") {
			@Override
			public String getFormat() {
				return Settings.Spy.FORMAT_BOOK;
			}

			@Override
			public String getLocalized() {
				return Lang.of("Commands.Spy.Type_Book");
			}
		},

		/**
		 * Spying items when renamed
		 */
		ANVIL("anvil") {
			@Override
			public String getFormat() {
				return Settings.Spy.FORMAT_ANVIL;
			}

			@Override
			public String getLocalized() {
				return Lang.of("Commands.Spy.Type_Anvil");
			}
		},

		;

		/**
		 * The saveable non-obfuscated key
		 */
		@Getter
		private final String key;

		/**
		 * Return the format used for the given spy type
		 *
		 * @return
		 */
		public abstract String getFormat();

		/**
		 * The messages_en.yml yummy dummy key
		 *
		 * @return
		 */
		public abstract String getLocalized();

		/**
		 * Attempt to load this class from the given config key
		 *
		 * @param key
		 * @return
		 */
		public static Type fromKey(String key) {
			for (final Type mode : values())
				if (mode.key.equalsIgnoreCase(key))
					return mode;

			throw new IllegalArgumentException("No such spying type: " + key + ". Available: " + Common.join(values()));
		}

		/**
		 * Returns {@link #getKey()}
		 */
		@Override
		public String toString() {
			return this.key;
		}
	}
}
