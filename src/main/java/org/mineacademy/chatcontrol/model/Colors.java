package org.mineacademy.chatcontrol.model;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.mineacademy.chatcontrol.PlayerCache;
import org.mineacademy.fo.PlayerUtil;
import org.mineacademy.fo.remain.CompChatColor;

/**
 * Class holding color-related utilities
 */
public final class Colors {

	/**
	 * Return the message with colors applied
	 *
	 * @param player
	 * @param message
	 * @return
	 */
	public static String colorizeMessage(CommandSender sender, String message) {
		// Set chat color and decoration
		if (sender instanceof Player) {
			final PlayerCache cache = PlayerCache.from((Player) sender);

			if (cache.hasChatDecoration())
				message = cache.getChatDecoration() + message;

			if (cache.hasChatColor())
				message = cache.getChatColor() + message;
		}

		// Set colors
		for (final CompChatColor color : CompChatColor.values())
			if (PlayerUtil.hasPerm(sender, Permissions.Color.LETTER.replace("{color}", color.getName())))
				message = message.replace("&" + color.getCode(), color.toString());

		return message;
	}

	/**
	 * Compile the permission for the color, if HEX or not, for the given sender
	 *
	 * @param sender
	 * @param color
	 * @return
	 */
	public static String getGuiColorPermission(CommandSender sender, CompChatColor color) {
		final String name = color.getName();

		if (name.startsWith("#"))
			return Permissions.Color.GUI_HEX.replace("{color}", name.substring(1));

		return Permissions.Color.GUI.replace("{color}", name);
	}

	/**
	 * Return list of colors the sender has permission for
	 *
	 * @param sender
	 * @return
	 */
	public static List<CompChatColor> getGuiColorsForPermission(CommandSender sender) {
		return loadGuiColorsForPermission(sender, CompChatColor.getColors());
	}

	/**
	 * Return list of decorations the sender has permission for
	 *
	 * @param sender
	 * @return
	 */
	public static List<CompChatColor> getGuiDecorationsForPermission(CommandSender sender) {
		return loadGuiColorsForPermission(sender, CompChatColor.getDecorations());
	}

	/*
	 * Compile list of colors the sender has permission to use
	 */
	private static List<CompChatColor> loadGuiColorsForPermission(CommandSender sender, List<CompChatColor> list) {
		final List<CompChatColor> selected = new ArrayList<>();

		for (final CompChatColor color : list)
			if (PlayerUtil.hasPerm(sender, Permissions.Color.GUI.replace("{color}", color.getName())))
				selected.add(color);

		return selected;
	}
}
