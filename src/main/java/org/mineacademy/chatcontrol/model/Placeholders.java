package org.mineacademy.chatcontrol.model;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.mineacademy.chatcontrol.PlayerCache;
import org.mineacademy.chatcontrol.operator.Tag;
import org.mineacademy.chatcontrol.settings.Lang;
import org.mineacademy.chatcontrol.settings.Settings;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.SerializeUtil;
import org.mineacademy.fo.model.HookManager;
import org.mineacademy.fo.model.SimpleExpansion;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/**
 * Dynamically insert data variables for PlaceholderAPI
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Placeholders extends SimpleExpansion {

	/**
	 * The singleton of this class
	 */
	@Getter
	private static final SimpleExpansion instance = new Placeholders();

	/**
	 * @see org.mineacademy.fo.model.SimpleExpansion#onReplace(org.bukkit.command.CommandSender, java.lang.String)
	 */
	@Override
	protected String onReplace(@NonNull CommandSender sender, String identifier) {
		final Player player = sender instanceof Player && ((Player) sender).isOnline() ? (Player) sender : null;
		final PlayerCache cache = player != null ? PlayerCache.from((Player) sender) : null;

		// Do not support offline players
		if (player == null)
			return NO_REPLACE;

		switch (identifier) {
			case "label_channel":
				return Settings.Channels.COMMAND_ALIASES.get(0);
			case "label_ignore":
				return Settings.Ignore.COMMAND_ALIASES.get(0);
			case "label_mail":
				return Settings.Mail.COMMAND_ALIASES.get(0);
			case "label_me":
				return Settings.Me.COMMAND_ALIASES.get(0);
			case "label_mute":
				return Settings.Mute.COMMAND_ALIASES.get(0);
			case "label_motd":
				return Settings.Motd.COMMAND_ALIASES.get(0);
			case "label_tag":
				return Settings.Tag.COMMAND_ALIASES.get(0);
			case "label_reply":
				return Settings.PrivateMessages.REPLY_ALIASES.get(0);
			case "label_spy":
				return Settings.Spy.COMMAND_ALIASES.get(0);
			case "label_tell":
				return Settings.PrivateMessages.TELL_ALIASES.get(0);
			case "label_toggle":
				return Settings.Toggle.COMMAND_ALIASES.get(0);
		}

		if ("channel".equals(identifier) && Settings.Channels.ENABLED && !Settings.Channels.IGNORE_WORLDS.contains(((Player) sender).getWorld().getName())) {
			final Channel writeChannel = cache.getWriteChannel();

			return writeChannel != null ? writeChannel.getName() : Lang.of("None").toLowerCase();
		}

		else if ("nick".equals(identifier) || "player_nick".equals(identifier))
			return Settings.Tag.APPLY_ON.contains(Tag.Type.NICK) ? Common.getOrDefaultStrict(cache.getTag(Tag.Type.NICK), player.getName()) : HookManager.getNickColored(sender);

		else if ("prefix_tag".equals(identifier) && Settings.Tag.APPLY_ON.contains(Tag.Type.PREFIX))
			return Common.getOrDefault(cache != null ? cache.getTag(Tag.Type.PREFIX) : null, "");

		else if ("suffix_tag".equals(identifier) && Settings.Tag.APPLY_ON.contains(Tag.Type.SUFFIX))
			return Common.getOrDefault(cache != null ? cache.getTag(Tag.Type.SUFFIX) : null, "");

		else if ((identifier.startsWith("receiver_is_spying_") || identifier.startsWith("receiver_in_channel_")) && Settings.Channels.ENABLED) {
			final String channelName = join(3);

			if (identifier.startsWith("receiver_is_spying_")) {
				if (!Settings.Spy.APPLY_ON.contains(Spy.Type.CHAT) || cache.isInChannel(channelName))
					return Lang.of("False_Value");

				return String.valueOf(cache.isSpyingChannel(channelName) || cache.getSpyingSectors().contains(Spy.Type.CHAT));
			}

			else {
				return String.valueOf(cache.isInChannel(channelName));
			}
		}

		if (args.length > 1 && "data".equalsIgnoreCase(args[0])) {
			final String key = join(1);
			final Object value = cache.getRuleData(key);

			return value != null ? SerializeUtil.serialize(value).toString() : "";
		}

		return NO_REPLACE;
	}
}
