package org.mineacademy.chatcontrol;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Nullable;

import org.mineacademy.chatcontrol.model.UserMap;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.Valid;
import org.mineacademy.fo.collection.SerializedMap;

import lombok.Getter;

/**
 * Represents a cache with data from BungeeCord
 */
@Getter
public final class SyncedCache {

	/**
	 * The internal map
	 */
	private static volatile Map<String, SyncedCache> cacheMap = new HashMap<>();

	/**
	 * The player name
	 */
	private final String playerName;

	/**
	 * The server where this player is on
	 */
	private String serverName;

	/**
	 * The player's unique ID
	 */
	private UUID uniqueId;

	/**
	 * His nick if any
	 */
	@Nullable
	private String nick;

	/**
	 * Is the player vanished?
	 */
	private boolean vanished;

	/**
	 * Is the player a fucking drunk?
	 */
	private boolean afk;

	/*
	 * Create a synced cache from the given data map
	 */
	private SyncedCache(String playerName, SerializedMap data) {
		Valid.checkBoolean(!data.isEmpty(), "Cannot decompile empty data!");

		this.playerName = playerName;
		this.serverName = data.getString("Server");
		this.uniqueId = data.get("UUID", UUID.class);
		this.nick = data.getString("Nick");
		this.vanished = data.getBoolean("Vanished");
		this.afk = data.getBoolean("Afk");
	}

	/**
	 * Return a dude's name or nick if set
	 *
	 * @return
	 */
	public String getNameOrNickColorless() {
		return Common.stripColors(this.getNameOrNickColored());
	}

	/**
	 * Return a dude's name or nick if set
	 *
	 * @return
	 */
	public String getNameOrNickColored() {
		return Common.getOrDefaultStrict(this.nick, this.playerName);
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SyncedCache{" + this.playerName + ",nick=" + this.nick + "}";
	}

	/* ------------------------------------------------------------------------------- */
	/* Static */
	/* ------------------------------------------------------------------------------- */

	/**
	 * Return true if the given player is connected and synced on BungeeCord
	 *
	 * @param playerName
	 * @return
	 */
	public static boolean isPlayerConnected(String playerName) {
		synchronized (cacheMap) {
			return cacheMap.containsKey(playerName);
		}
	}

	/**
	 * Return true if the given player is connected and synced on BungeeCord
	 *
	 * @param playerUUID
	 * @return
	 */
	public static boolean isPlayerConnected(UUID playerUUID) {
		synchronized (cacheMap) {
			final String playerName = UserMap.getInstance().getName(playerUUID);

			return playerName != null && cacheMap.containsKey(playerName);
		}
	}

	/**
	 * Return the synced cache (or null) from the player name
	 *
	 * @param playerName
	 * @return
	 */
	@Nullable
	public static SyncedCache fromName(String playerName) {
		return cacheMap.get(playerName);
	}

	/**
	 * Return a set of all known servers on BungeeCord where players are on
	 *
	 * @return
	 */
	public static Set<String> getServers() {
		synchronized (cacheMap) {
			final Set<String> servers = new HashSet<>();

			for (final SyncedCache cache : getCaches())
				servers.add(cache.getServerName());

			return servers;
		}
	}

	/**
	 * Return all caches stored in memory
	 *
	 * @return
	 */
	public static Collection<SyncedCache> getCaches() {
		synchronized (cacheMap) {
			return cacheMap.values();
		}
	}

	/**
	 * Retrieve (or create) a sender cache
	 *
	 * @param senderName
	 * @return
	 */
	public static void upload(SerializedMap data) {
		synchronized (cacheMap) {
			cacheMap.clear();

			for (final Map.Entry<String, Object> entry : data.entrySet()) {
				final String playerName = entry.getKey();
				final SerializedMap playerData = SerializedMap.fromJson(entry.getValue().toString());

				cacheMap.put(playerName, new SyncedCache(playerName, playerData));
			}
		}
	}
}
